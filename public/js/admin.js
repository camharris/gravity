
function userDelete(username) {

	var user_info = {};

	user_info["username"] = username;

	var message = {
		"msg_type" : "request",
		"command" : "user_delete",
		"user_info" : user_info
	}

	console.log("sending message = " + JSON.stringify(message))

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {

			console.log( "Success: Got back response that we deleted a user" )
			location.reload();
		},
		error: function(response) {
			console.log( "userDelete: Got back an error" )

		}
	});
}

function userLogout(username) {

	var user_info = {};
	user_info["username"] = username;

	var message = {
		"msg_type" : "request",
		"command" : "user_logout",
		"user_info" : user_info
	}

	console.log("sending message = " + JSON.stringify(message))

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {

			console.log( "Success: Got back response that we logged the user out" )
			location.reload();
		},
		error: function(response) {
			console.log( "userLogout: Got back an error" )

		}
	});

}

// This function sends a message to the server to add a user to the system.
function userAddNew() {
	// TODO: This function is tied to users.html.  Should make it generic and pass in an object

	// Clear any previous errors before we continue
	clearError();

	var user_info = {}

	user_info["username"] = $("#modal_username").val();
	user_info["first_name"] = $("#modal_first_name").val();
	user_info["last_name"] = $("#modal_last_name").val();
	user_info["password"] = $("#modal_password").val();
	user_info["password_confirm"] = $("#modal_password_confirm").val();

	user_info["permissions"] = {}

	// Default everything to true
	user_info["permissions"]["admin"] = true;
	user_info["permissions"]["web"] = true;

	if( $('#adminCheckBox').is(':checked') == false ) {
		user_info["permissions"]["admin"] = false;
	}

	if( $('#webCheckBox').is(':checked') == false ) {
		user_info["permissions"]["web"] = false;
	}

	var message = {
		"msg_type" : "request",
		"command" : "user_add_new",
		"user_info" : user_info
	}

	console.log("sending message = " + JSON.stringify(message))

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {

			console.log( "Success: Got back response that we created a user" )
			location.reload();
		},
		error: function(response) {
			console.log( "userAddNew: Got back an error" )

		}
	});

}

function userEdit(userInfo) {

	var message = {
		"msg_type" : "request",
		"command" : "user_edit",
		"user_info" : userInfo
	}

	console.log("sending message = " + JSON.stringify(message))

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {

			console.log( "Success: Got back response that we edited the user" )
			location.reload();
		},
		error: function(response) {
			console.log( "userEdit: Got back an error" )

		}
	});

}

// This function is called by a client that may want to change the
//  password for themselves or another user.  Must validate that they
//  have that permission on the server.
function userPasswordEdit(userInfo) {

	var message = {
		"msg_type" : "request",
		"command" : "user_edit_password",
		"user_info" : userInfo
	}

	console.log("sending message = " + JSON.stringify(message))

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {

			console.log( "Success: Got back response that we edited the user password" )
		},
		error: function(response) {
			console.log( "userEdit: Got back an error" )

		}
	});

}

function clearError() {
	$("#errors").hide();
}