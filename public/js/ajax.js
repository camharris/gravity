// This file supports AJAX calls to the gravity server.

function test_ajax() {

	let ajax_test_input = $("#ajax_test_input").val()

	let message = {
		"msg_type" : "request",
		"command" : "ajax_test",
		"value" : ajax_test_input
	}

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {
			console.log( "Got back a response from our AJAX request, response = " + response )

			if ( "output" in response ) {
				$("#ajax_test_output").html( response["output"] )
			}

			$("div#ajax_output_alert").show()
		},
		error: function(response) {
			console.log("got back an error")
		}
	});
}

function sendBlogNewPost(title, subtitle, text) {

	var r = confirm("Are you sure you want to submit your post?");
	if (r == false) {
	  return;
	}

	let blog_new_post_title = $("#blog_new_post_title").val()
	let blog_new_post_subtitle = $("#blog_new_post_subtitle").val()
	let blog_new_post_text = $("#blog_new_post_text").val()

	let message = {
		"msg_type" : "request",
		"command" : "blog_new_post",
		"blog_item" : {
			"title" : blog_new_post_title,
			"subtitle" : blog_new_post_subtitle,
			"text" : blog_new_post_text
		}
	}

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {
			console.log( "Got back a response from our AJAX request, response = " + response )

			alert ( "New blog post submitted" )

			window.location = "/blog";
		},
		error: function(response) {
			console.log("got back an error")
		}
	});
}

function submitBlogEdit() {

	let blog_edit_title = $("#blog_edit_post_title").val()
	let blog_edit_subtitle = $("#blog_edit_post_subtitle").val()
	let blog_edit_text = $("#blog_edit_post_text").val()
	let blog_edit_label = $("#blog_edit_label").val()

	blog_edit_label

	let message = {
		"msg_type" : "request",
		"command" : "blog_submit_edit",
		"blog_item" : {
			"title" : blog_edit_title,
			"label" : blog_edit_label,
			"subtitle" : blog_edit_subtitle,
			"text" : blog_edit_text
		}
	}

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {
			console.log( "Got back a response from our AJAX request, response = " + response )

			alert ( "Edit event submitted" )

			//window.location = "/events";
		},
		error: function(response) {
			console.log("Got back an error")
		}
	});
}

function submitBlogPostDeleteHandler(){

	let blog_label = $("#edit_event_label").val()

	submitBlogPostDelete( blog_label, "/events" )
}

function submitBlogPostDelete(blogLabel, redirectLocation) {

	console.log( "submitEventDelete: Deleting blog post " + blogLabel )

	let message = {
		"msg_type" : "request",
		"command" : "blog_submit_delete",
		"blog_item" : {
			"label" : blogLabel
		}
	}

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {
			console.log( "Got back a response from our AJAX request, response = " + response )

			alert ( "Delete event submitted" )

			window.location = redirectLocation;
		},
		error: function(response) {
			console.log("Got back an error")
		}
	});
}

// ***** Gravity Event Functions *****

function submitNewEvent(title, subtitle, text) {

	var r = confirm("Are you sure you want to submit this event?");
	if (r == false) {
	  return;
	}

	let event_new_title = $("#event_new_title").val()
	let event_new_location = $("#event_new_location").val()
	let event_new_datetime = $("#event_new_datetime").val()
	let event_new_description = $("#event_new_description").val()

	let message = {
		"msg_type" : "request",
		"command" : "event_submit_new",
		"event_item" : {
			"title" : event_new_title,
			"location" : event_new_location,
			"datetime" : event_new_datetime,
			"description" : event_new_description
		}
	}

	message_request = JSON.stringify( message )
	console.log( message_request )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {
			console.log( "Got back a response from our AJAX request, response = " + response )

			alert ( "New event submitted" )

			window.location = "/events";
		},
		error: function(response) {
			console.log("Got back an error")
		}
	});
}

function submitEventEdit(title, subtitle, text) {

	let event_edit_title = $("#event_edit_title").val()
	let event_edit_label = $("#event_edit_label").val()
	let event_edit_location = $("#event_edit_location").val()
	let event_edit_datetime = $("#event_edit_datetime").val()
	let event_edit_description = $("#event_edit_description").val()

	let message = {
		"msg_type" : "request",
		"command" : "event_submit_edit",
		"event_item" : {
			"label" : event_edit_label,
			"title" : event_edit_title,
			"location" : event_edit_location,
			"datetime" : event_edit_datetime,
			"description" : event_edit_description
		}
	}

	message_request = JSON.stringify( message )

	console.log( message_request )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {
			console.log( "Got back a response from our AJAX request, response = " + response )

			alert ( "Edit event submitted" )

			//window.location = "/events";
		},
		error: function(response) {
			console.log("Got back an error")
		}
	});
}

function submitEventDeleteHandler(){

	let event_label = $("#edit_event_label").val()

	submitEventDelete( event_label, "/events" )
}

function submitEventDelete(eventLabel, redirectLocation) {

	console.log( "submitEventDelete: Deleting event " + eventLabel )

	let message = {
		"msg_type" : "request",
		"command" : "event_submit_delete",
		"event_item" : {
			"label" : eventLabel
		}
	}

	message_request = JSON.stringify( message )

	// Sending AJAX Request to logout.
	$.ajax({
		type: 'POST',
		url: "/api",
		contentType: "application/json",
		data: message_request,
		// Fetch the stored token from localStorage and set in the header
		success: function(response) {
			console.log( "Got back a response from our AJAX request, response = " + response )

			alert ( "Edit event submitted" )

			window.location = redirectLocation;
		},
		error: function(response) {
			console.log("Got back an error")
		}
	});
}