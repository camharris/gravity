#!/usr/bin/python

"""
Client code that interacts with the web server over AJAX requests.

Written by: J. Patrick Farrell
Copyright 2018 Creative Collisions Technology, LLC

"""

import requests, json

class GravityAjaxApi():

	def __init__(self):

		self.url = "http://localhost"

	def ajaxTest(self):

		header = {"Content-Type": "application/json"}
		payload = { "command": "ajax_test", "value": "TEST1234" }

		response_decoded_json = requests.post(self.url, data=json.dumps(payload), headers=header)
		response_json = response_decoded_json.json()

		print response_json

if __name__ == "__main__":

	gravity_api = GravityAjaxApi()
	gravity_api.ajaxTest()