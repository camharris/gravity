#!/usr/bin/env python

"""
Written by: J. Patrick Farrell
Copyright 2019 Creative Collisions Technology, LLC

MessageHandlers.py

These are the message handlers that take care of JSON requests coming in.

Note: We had to create this as a class and pass in JsonMessage on purpose
because we cannot import JsonMessage because it doesn't work.

"""

# **** Local Imports ****
from GravityBlog import *
from GravityEvents import *
from UserManagement import *

MSG_TYPE_REPLY = "reply"

class MessageHandlers():

	def __init__(self, JsonMessage):
		self.json_message = JsonMessage

	def handleAjaxTest(self, messageJson, messageResponse, logger=None):

		command = messageJson["command"]
		message_response = messageResponse

		if "value" in messageJson:
			value = messageJson["value"]

			output = "Gravity Server Received Input: %s" % value
			message_response["output"] = output

		return message_response

	# **** Blog Handlers ****

	def handleSetBlogNewPost(self, messageJson, logger):
		"""
		This function handles create a new blog entry in the blog system.
		"""

		message_response = self.json_message.createResponseMessage( "blog_new_post" )

		if "blog_item" in messageJson:

			blog_item = messageJson["blog_item"]

			title = blog_item["title"]
			subtitle = blog_item["subtitle"]
			text = blog_item["text"]

			gravity_blog = GravityBlog()
			result = gravity_blog.createPost( title, subtitle, text )

			if result == None:
				error_reason = "handleSetBlogNewPost: Error creating new blog entry"
				logger.error( error_reason )
				message_response = self.json_message.createErrorMessage( "reply", "blog_new_post", error_reason )

		else:
			error_reason = "handleSetBlogNewPost: blog_item object not found"
			logger.error( error_reason )
			message_response = self.json_message.createErrorMessage( "reply", "blog_new_post", error_reason )

		return message_response

	def handleBlogSubmitEdit(self, messageJson, logger):
		"""
		Handles submission of a blog edit.
		"""

		message_response = self.json_message.createResponseMessage( "blog_submit_edit" )

		if "blog_item" in messageJson:

			blog_item = messageJson["blog_item"]

			gravity_blog = GravityBlog()
			result = gravity_blog.editPost( blog_item )

			if result == None:
				error_reason = "handleBlogSubmitEdit: Error creating new blog entry"
				logger.error( error_reason )
				message_response = self.json_message.createErrorMessage( "reply", "blog_submit_edit", error_reason )

		else:
			error_reason = "handleBlogSubmitEdit: blog_item object not found"
			logger.error( error_reason )
			message_response = self.json_message.createErrorMessage( "reply", "blog_submit_edit", error_reason )

		return message_response

	def handleBlogSubmitDelete(self, messageJson, logger):
		"""
		This function handles editing an event.
		"""

		message_response = self.json_message.createResponseMessage( "blog_submit_delete" )

		if "blog_item" in messageJson:

			blog_item = messageJson["blog_item"]

			if "label" not in blog_item:
				error_reason = "handleBlogSubmitDelete: Error blog label not found"
				logger.error( error_reason )
				message_response = self.json_message.createErrorMessage( "reply", "blog_submit_delete", error_reason )
			else:

				blog_label = blog_item["label"]

				gravity_blog = GravityBlog()
				result = gravity_blog.deletePost( blog_label )

				if result == None:
					error_reason = "handleBlogSubmitDelete: Error submitting delete blog post"
					logger.error( error_reason )
					message_response = self.json_message.createErrorMessage( "reply", "blog_submit_delete", error_reason )

		else:
			error_reason = "handleBlogSubmitDelete: event_item object not found"
			logger.error( error_reason )
			message_response = self.json_message.createErrorMessage( "reply", "event_submit_delete", error_reason )

		return message_response

	# **** Event Handlers *****
	def handleEventSubmitNew(self, messageJson, logger):
		"""
		This function handles create a new blog entry in the blog system.
		"""

		message_response = self.json_message.createResponseMessage( "event_submit_new" )

		if "event_item" in messageJson:

			event_item = messageJson["event_item"]

			# TODO: Check for items.
			title = event_item["title"]
			location = event_item["location"]
			datetime = event_item["datetime"]
			description = event_item["description"]

			gravity_events = GravityEvents()
			print "calling createEvent with title %s" % title
			result = gravity_events.createEvent( title, datetime, location, description )

			if result == None:
				error_reason = "handleSubmitNewEvent: Error submitting new event"
				logger.error( error_reason )
				message_response = self.json_message.createErrorMessage( "reply", "event_submit_new", error_reason )

		else:
			error_reason = "handleSubmitNewEvent: blog_item object not found"
			logger.error( error_reason )
			message_response = self.json_message.createErrorMessage( "reply", "event_submit_new", error_reason )

		return message_response

	def handleEventSubmitEdit(self, messageJson, logger):
		"""
		This function handles editing an event.
		"""

		print "inside handleEventSubmitEdit "
		message_response = self.json_message.createResponseMessage( "event_submit_edit" )

		if "event_item" in messageJson:

			event_item = messageJson["event_item"]

			gravity_events = GravityEvents()
			result = gravity_events.editEvent( event_item )

			if result == None:
				error_reason = "handleEventSubmiEdit: Error submitting new event"
				logger.error( error_reason )
				message_response = self.json_message.createErrorMessage( "reply", "event_submit_edit", error_reason )

		else:
			error_reason = "handleEventSubmiEdit: event_item object not found"
			logger.error( error_reason )
			message_response = self.json_message.createErrorMessage( "reply", "event_submit_edit", error_reason )

		return message_response

	def handleEventSubmitDelete(self, messageJson, logger):
		"""
		This function handles editing an event.
		"""

		message_response = self.json_message.createResponseMessage( "event_submit_delete" )

		if "event_item" in messageJson:

			event_item = messageJson["event_item"]

			if "label" not in event_item:
				error_reason = "handleEventSubmitDelete: Error event label not found"
				logger.error( error_reason )
				message_response = self.json_message.createErrorMessage( "reply", "event_submit_delete", error_reason )
			else:

				event_label = event_item["label"]

				gravity_events = GravityEvents()
				result = gravity_events.deleteEvent( event_label )

				if result == None:
					error_reason = "handleEventSubmitDelete: Error submitting delete event"
					logger.error( error_reason )
					message_response = self.json_message.createErrorMessage( "reply", "event_submit_delete", error_reason )

		else:
			error_reason = "handleEventSubmitDelete: event_item object not found"
			logger.error( error_reason )
			message_response = self.json_message.createErrorMessage( "reply", "event_submit_delete", error_reason )

		return message_response

	# **** User Management Functions ****
	def handleUserAddNew(self, messageJson, logger):
		"""
		This function takes in a message that sets a new user into the system.

		"""
		command = "user_add_new"

		if "user_info" in messageJson:

			user_info = messageJson["user_info"]

			# TODO: Make sure the new user object matches the JSON schema.

			if "username" not in user_info or "password" not in user_info or "first_name" not in user_info or "last_name" not in user_info or "permissions" not in user_info:
				message_response = self.json_message.createErrorMessage( command, "Warning: Incomplete information, cannot add new user" )
				return message_response

			# Store the new user in the system.
			user_info_confirmed = UserManagement.addNewUser( user_info )

			if user_info_confirmed != None:
				logger.info( "Successfully created new user %s %s (%s)" % ( user_info["first_name"], user_info["last_name"], user_info["username"] ) )
				# Success in adding a user, create a response
				message_response = self.json_message.createResponseMessage(command)
				message_response["user_info"] = user_info_confirmed
			else:
				logger.error( "Failed to create new user" )
				# TODO: Add the right reason, passwords don't match or we already have this user.
				message_response = self.json_message.createErrorMessage( command, "Warning: We could not create the new user." )

			return message_response

	def handleUserDelete(self, messageJson, logger):
		"""
		This function deletes a user if the user is present in the system
		"""
		command = "user_delete"

		if "user_info" in messageJson:

			user_info = messageJson["user_info"]

			# Store the new user in the system.
			user_info_delete_confirmed = UserManagement.deleteUser( user_info )

			if user_info_delete_confirmed != None:
				logger.info( "Successfully deleted user %s" % user_info["username"] )
				# Success in adding a user, create a response
				message_response = self.json_message.createResponseMessage(command)
				message_response["user_info"] = user_info_delete_confirmed
			else:
				# Tell the user we couldn't find the user to delete
				logger.info( "Failed to delete user" )
				message_response = self.json_message.createErrorMessage( command, "Warning: We could not delete user %s" % user_info["username"] )

		else:
			message_response = self.json_message.createErrorMessage( command, "Warning: We could not find user_info object" )

		return message_response

	def handleUserEdit(self, messageJson, logger):
		"""
		This function edits a user in the system.  Pass in a user_info object with the username and any new information.

		Currently you cannot change the username, to do this, just create a new user.  If required, could add.
		"""
		command = "user_edit"

		if "user_info" in messageJson:

			user_info = messageJson["user_info"]

			# Store the new user in the system.
			user_info_edit_confirmed = UserManagement.editUser( user_info )

			if user_info_edit_confirmed != None:
				logger.info( "User information edited successfully for %s" % user_info["username"] )
				# Success in adding a user, create a response
				message_response = self.json_message.createResponseMessage(command)
				message_response["user_info"] = user_info_edit_confirmed
			else:
				# Tell the user we couldn't find the user to delete
				error_message =  "We could not delete user %s" % user_info["username"]
				logger.error( error_message )
				message_response = self.json_message.createErrorMessage( command, error_message )

		else:
			error_message =  "We could not find user_info object"
			logger.error( error_message )
			message_response = self.json_message.createErrorMessage( command, error_message  )

		return message_response

	def handleUserEditPassword(self, messageJson, logger):
		"""
		This function is a password.
		"""
		command = "user_password_edit"

		if "user_info" in messageJson:

			user_info = messageJson["user_info"]
			username = ""

			if "username" in user_info:
				username = user_info["username"]

			# Store the new user in the system.
			user_info_edit_password_confirmed = UserManagement.editUserPassword( user_info )

			if user_info_edit_password_confirmed != None:
				# Success in editing user password.
				logger.info( "Successfully changed the password for %s" % username )
				message_response = self.json_message.createResponseMessage(command)
				message_response["user_info"] = user_info_edit_password_confirmed
			else:
				# Tell the user we couldn't find the user to delete
				logger.info( "Failed to change the password for %s" % username )
				message_response = self.json_message.createErrorMessage( command, "Error: We could not change the user password for %s" % username )

		else:
			message_response = self.json_message.createErrorMessage( command, "Error: We could not find user_info object" )

		return message_response

	def handleUserLogout(self, messageJson, logger):
		"""
		This function logs a particular user out.  Only admins should be allowed to call this function.

		TODO: Verify that the current user submitting this request has permissions to perform this action.
		"""
		command = "user_logout"

		if "user_info" in messageJson and "username" in messageJson["user_info"]:

			user_info = messageJson["user_info"]
			username = ""

			if "username" in user_info:
				username = user_info["username"]

			result = UserManagement.revokeUserTokens( username )

			if result == True:
				logger.info( "Logged out user %s" % username )
				message_response = self.json_message.createResponseLogoutMessage( user_info )
			else:
				logger.error( "Failed to logout user %s" % username )
				message_response = self.json_message.createErrorMessage( command, "We couldn't logout user  %s" % user_info["username"] )

		else:
			logger.warning( "We could not find username" )
			message_response = self.json_message.createErrorMessage( command, "Warning: We could not find username" )

		return message_response
