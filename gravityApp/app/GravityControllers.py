#!/usr/bin/env python

"""
Written by: J. Patrick Farrell
Copyright 2019 Creative Collisions Technology, LLC

These are the controllers that render specific HTML templates for the application routes.

This is server-side functionality allows someone to create a template and configure the HTML that is sent
back to the user.

"""

import datetime
from jinja2 import Environment, FileSystemLoader # Templating Engine
import os
import json

# **** Local Includes ****
from GravityBlog import *
from GravityEvents import *
from GravityConfiguration import *
from UserManagement import *

env = Environment(
	loader=FileSystemLoader(TEMPLATES_ROOT_DIRECTORY)
)

class GravityPathHandler():

	@staticmethod
	def getController(path):
		"""
		This function determines which controller needs to be returned, if a controller doesn't
		match the path, then return None.

		When the user wants to add a new controller for a new route, they need to add a new path to this
		function and they need to add a new controller function to the GravityControllers class.

		TODO: This should be a dictionary or some data structure to make this cleaner.
		"""

		full_path = path

		path_array = path.split( "/" )

		if len( path_array ) >= 2:
			path = path_array[1]
		else:
			return None

		if path == "" or path == "home.html" or path == "index.html":
			return GravityControllers.home
		elif path == "about":
			return GravityControllers.about
		elif path == "admin":
			# Use the sub-function to find the admin controller to use.
			return GravityPathHandler.getControllerAdmin(full_path)
		elif path == "blog":
			return GravityControllers.blog
		elif path == "blognewpost":            # TODO: Would like this to be /blog/new but it breaks GETs for other files.
			return GravityControllers.blogNewPost
		elif path == "events":
			if len( path_array ) >= 3 and path_array[2] == "new":
				return GravityControllers.eventNew
			elif len( path_array ) >= 3 and path_array[2] == "manage":
				return GravityControllers.eventsManage
			else:
				return GravityControllers.events
		elif path == "event":
			if len( path_array ) >= 3 and path_array[2] == "edit":
				return GravityControllers.eventEdit
			else:
				return GravityControllers.event
		elif path == "features":
			return GravityControllers.features
		elif path == "forgot-password" or path == "forgot-password.html" :
			return GravityControllers.forgotPassword
		elif path == "login" or path == "login.html" :
			return GravityControllers.login
		elif path == "upload" or path == "upload.html" :
			return GravityControllers.upload
		elif path == "register" or path == "register.html" :
			return GravityControllers.register
		else:
			return None

	@staticmethod
	def getControllerAdmin(path):
		"""
		This function determines which admin controller should be returned.

		Typically this part of the website will have a different layout and theme than the
		main site.
		"""

		path_array = path.split( "/" )

		if len( path_array ) >= 2:
			path = path_array[1]
		else:
			return None

		if len( path_array ) >= 4:
			if path_array[2] == "blog":
				if path_array[3] == "new" or path_array[3] == "new.html":
					return GravityControllersAdmin.blogNew
				elif path_array[3] == "edit" or path_array[3] == "edit.html":
					return GravityControllersAdmin.blogEdit
				elif path_array[3] == "manage" or path_array[3] == "manage.html":
					return GravityControllersAdmin.blogManage
			elif path_array[2] == "events" or path_array[2] == "event":
				if path_array[3] == "new" or path_array[3] == "new.html":
					return GravityControllersAdmin.eventNew
				elif path_array[3] == "edit" or path_array[3] == "edit.html":
					return GravityControllersAdmin.eventEdit
				elif path_array[3] == "manage" or path_array[3] == "manage.html":
					return GravityControllersAdmin.eventsManage
			elif path_array[2] == "users":
				if path_array[3] == "manage" or path_array[3] == "manage.html":
					return GravityControllersAdmin.usersManage
		elif len( path_array ) >= 3:
			if path_array[2] == "index" or path_array[2] == "index.html":
				return GravityControllersAdmin.admin
			if path_array[2] == "login" or path_array[2] == "login.html":
				return GravityControllersAdmin.login
			elif path_array[2] == "register" or path_array[2] == "register.html":
				return GravityControllersAdmin.register
			elif path_array[2] == "forgot-password" or path_array[2] == "forgot-password.html":
				return GravityControllersAdmin.forgotPassword
		else:
			return GravityControllersAdmin.admin

class GravityControllersAdmin():
	"""
	This class is for controllers on the administrator part of the site.

	Note: For any page that requires the user to be logged in, we need to call
		the getContext function so we have information about the permissions this user has.
	"""

	@staticmethod
	def getContext(path, currentUser):

		context = {}

		# TODO: We should look up the username and password
		context["current_user"] = currentUser

		if UserManagement.isAdmin( currentUser ) == True:
			context["admin"] = True

		return context

	@staticmethod
	def admin(path, currentUser=None):
		"""
		Gets the admin template.
		"""

		context = GravityControllersAdmin.getContext( path, currentUser )

		return GravityControllers.render_template( "admin/index.j2", context )

	@staticmethod
	def login(path, currentUser=None):
		"""
		Gets the admin template.
		"""
		return GravityControllers.render_template( "admin/auth/login.j2" )

	@staticmethod
	def register(path, currentUser=None):
		"""
		Gets the admin template.
		"""
		return GravityControllers.render_template( "admin/auth/register.j2" )

	@staticmethod
	def forgotPassword(path, currentUser=None):
		"""
		Gets the admin template.
		"""
		return GravityControllers.render_template( "admin/auth/forgot-password.j2" )

	@staticmethod
	def blogManage(path, currentUser=None):

		context = GravityControllersAdmin.getContext( path, currentUser )

		gravity_blog = GravityBlog()
		blog_items = gravity_blog.getBlogItems()

		if blog_items == None:
			blog_items = []

		context["blog_items"] = blog_items

		return GravityControllers.render_template( "admin/blog/manage.j2", context )

	@staticmethod
	def blogNew(path, currentUser=None):

		context = GravityControllersAdmin.getContext( path, currentUser )

		return GravityControllers.render_template( "admin/blog/new.j2", context )

	@staticmethod
	def blogEdit(path, currentUser=None):
		"""
		Gets the page to edit an event for the administrator.
		"""

		context = GravityControllersAdmin.getContext( path, currentUser )

		gravity_blog = GravityBlog()

		path_array = path.split( "/" )

		if len( path_array ) >= 3:
			path = path_array[2]
			blog_label = path_array[4]

			blog_item = gravity_blog.getBlogItem( blog_label )
		else:
			blog_item = None

		if blog_item == None:
			blog_item = {}

		context["blog_item"] = blog_item

		return GravityControllers.render_template( "/admin/blog/edit.j2", context )

	@staticmethod
	def eventsManage(path, currentUser=None):

		context = GravityControllersAdmin.getContext( path, currentUser )

		gravity_events = GravityEvents()
		event_items = gravity_events.getEventItems()

		if event_items == None:
			event_items = []

		context["event_items"] = event_items

		return GravityControllers.render_template( "admin/events/manage.j2", context )

	@staticmethod
	def eventNew(path, currentUser=None):

		context = GravityControllersAdmin.getContext( path, currentUser )

		return GravityControllers.render_template( "admin/events/new.j2", context )

	@staticmethod
	def eventEdit(path, currentUser=None):
		"""
		Gets the page to edit an event for the administrator.
		"""

		context = GravityControllersAdmin.getContext( path, currentUser )

		gravity_events = GravityEvents()

		path_array = path.split( "/" )

		if len( path_array ) >= 3:
			path = path_array[2]
			event_label = path_array[4]

			event = gravity_events.getEvent( event_label )
		else:
			event = None

		if event == None:
			event = {}

		context["event"] = event

		return GravityControllers.render_template( "/admin/events/edit.j2", context )

	@staticmethod
	def usersManage(path, currentUser=None):
		"""
		Gets the page to manage users as an administrator.
		"""

		context = GravityControllersAdmin.getContext( path, currentUser )

		user_items = UserManagement.getCredentials()

		if user_items == None:
			user_items = []

		context["user_items"] = user_items

		return GravityControllers.render_template( "admin/users/manage.j2", context )


class GravityControllers():

	@staticmethod
	def render_template(templateFile, context=None):

		if context == None:
			context = {}

		return env.get_template(templateFile).render(context)

	@staticmethod
	def home(path, currentUser=None):
		return GravityControllers.render_template( "index.j2" )

	@staticmethod
	def about(path, currentUser=None):

		paragraphs = []
		paragraphs.append( "To create a framework that makes learning the basics of how a web server works accessible to anyone.  The Gravity Framework will be built out as an open source framework and developers can build their own systems on top of it, ensuring they don't have to constantly re-invent the software wheel." )
		paragraphs.append( "This framework is built from scratch, show caseing how to use server-side templating, AJAX requests, websockets, user management, client-side rendering, software static analysis, integration with payment systems, and how to build and license open source software technology." )
		paragraphs.append( "By working with this framework, you will learn about how to use version control, specifically GIT.  You will learn how to work with a software team, you will learn how to create secure software, and you will have a framework that you can use in your own projects." )
		paragraphs.append( "Welcome to the Creative Collisions Tech Community and I hope this project gives you a lot of value for understanding web frameworks and gives you useful tools to succeed." )

		context = {
			'paragraphs': paragraphs
		}

		return GravityControllers.render_template( "about.j2", context )

	# **** Blog Functions ****

	@staticmethod
	def blog(path, currentUser=None):
		"""
		Get the blog items from the blog class and returns as a rendered template.

		Note that by doing this here, this means that the blog is currently generated server-side rather
		than client side.
		"""

		gravity_blog = GravityBlog()
		blog_items = gravity_blog.getBlogItems()

		if blog_items == None:
			blog_items = []

		context = {
			'blog_items': blog_items
		}

		return GravityControllers.render_template( "blog.j2", context )

	@staticmethod
	def blogNewPost(path, currentUser=None):
		"""
		Gets the page for a new blog post
		"""

		return GravityControllers.render_template( "blog/new.j2" )

	# **** Event Functions ****
	@staticmethod
	def events(path, currentUser=None):
		"""
		Get the blog items from the blog class and returns as a rendered template.

		Note that by doing this here, this means that the blog is currently generated server-side rather
		than client side.
		"""

		gravity_events = GravityEvents()
		event_items = gravity_events.getEventItems()

		if event_items == None:
			event_items = []

		context = {
			'event_items': event_items
		}

		return GravityControllers.render_template( "events/events.j2", context )

	@staticmethod
	def eventsManage(path, currentUser=None):
		"""
		This allows you to manage the events that you have in the system.
		"""

		gravity_events = GravityEvents()
		event_items = gravity_events.getEventItems()

		if event_items == None:
			event_items = []

		context = {
			'event_items': event_items
		}

		return GravityControllers.render_template( "events/manage.j2", context )

	@staticmethod
	def event(path, currentUser=None):
		"""
		Gets the page that will display an individual event.
		"""

		path_array = path.split("/")

		if len( path ) <= 2:
			return GravityControllers.render_template( "events/event.j2" )
		else:
			# We need to look up what event we are interested in displaying and return it
			#  in the context variable.
			event_label = path_array[2]

			gravity_events = GravityEvents()

			event = gravity_events.getEvent( event_label )

			if event == None:
				event = {}

			context = {
				'event': event
			}

			return GravityControllers.render_template( "events/event.j2", context )

	@staticmethod
	def eventNew(path, currentUser=None):
		"""
		Gets the page for a new blog post
		"""

		return GravityControllers.render_template( "events/new.j2" )

	@staticmethod
	def eventEdit(path, currentUser=None):
		"""
		Gets the page to edit an event
		"""

		gravity_events = GravityEvents()

		path_array = path.split( "/" )

		if len( path_array ) >= 2:
			path = path_array[1]
			event_label = path_array[3]

			event = gravity_events.getEvent( event_label )
		else:
			event = None

		if event == None:
			event = {}

		context = {
			'event': event
		}

		return GravityControllers.render_template( "events/edit.j2", context )

	@staticmethod
	def features(path, currentUser=None):

		bullets = []
		bullets.append( "AJAX support" )
		bullets.append( "WebSocket support" )
		bullets.append( "Full Docker Container Implementation" )
		bullets.append( "Server-Side Templating with Jinga2" )
		bullets.append( "Client Side Rendering" )
		bullets.append( "File Upload over HTTP POST" )
		bullets.append( "User Login and Administrator Management" )
		bullets.append( "JSON Web Token Support" )
		bullets.append( "JSON Application Programmer Interface (API)" )
		bullets.append( "Complete Offline Support Framework" )

		context = {
			'bullets': bullets
		}

		return GravityControllers.render_template( "features.j2", context )

	@staticmethod
	def forgotPassword(path, currentUser=None):
		"""
		TODO: Using admin forgot password for now until we create a user login page.
		"""
		#return GravityControllers.render_template( "auth/forgot-password.j2" )
		return GravityControllers.render_template( "admin/auth/forgot-password.j2" )

	@staticmethod
	def login(path, currentUser=None):
		"""
		TODO: Using admin login for now until we create a user login page.
		"""
		#return GravityControllers.render_template( "auth/login.j2" )
		return GravityControllers.render_template( "admin/auth/login.j2" )

	@staticmethod
	def register(path, currentUser=None):
		"""
		TODO: Using admin register for now until we create a user login page.
		"""
		#return GravityControllers.render_template( "auth/register.j2" )
		return GravityControllers.render_template( "admin/auth/register.j2" )

	@staticmethod
	def upload(path, currentUser=None):
		return GravityControllers.render_template( "upload.j2" )