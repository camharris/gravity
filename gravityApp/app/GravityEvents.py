#!/usr/bin/env python

"""

GravityEvents.py

This file manages Gravity Events.  This allow

Written by: J. Patrick Farrell
Copyright 2019

TODO: This is very basic right now, need to add event time storage, formatting and many other things.

"""

import datetime
import json

# **** Local Import ****
from JsonFile import *
from GravityConfiguration import *

DEFAULT_ALT_IMAGE = "/img/unsplash/jakob-dalbjorn-cuKJre3nyYc-unsplash.jpg"

class GravityEvents():

	def __init__(self):
		pass

	def createEvent(self, eventTitle, eventDateTime, eventLocation, eventDescription, eventImagePath=None):
		"""
		This function creates an event and saves it within a file called events.json
		"""

		events_file_path = GravityConfiguration.getEventsFile()
		current_events = JsonFile.readJsonFile( events_file_path )

		if current_events == None:
			current_events = {}

		event_item = {}
		event_item["datetime_created"] = datetime.datetime.now().ctime()
		event_item["title"] = eventTitle
		event_item["datetime"] = eventDateTime
		event_item["location"] = eventLocation
		event_item["description"] = eventDescription

		if eventImagePath != None:
			event_item["image"] = eventImagePath

		# Create the label that we are going to use for the URL.
		# TODO: We need to make this much more robust.
		label = eventTitle.lower().replace(" ", "-")
		label = label.replace("'", "-")
		label = label.replace(",", "-")
		event_item["label"] = label

		if "event_items" not in current_events:
			current_events["event_items"] = []

		current_events["event_items"].append( event_item )

		result = JsonFile.writeJsonFile( current_events, events_file_path )

		if result == False:
			reason = "Error: Could not write blog file"
			print reason
			return None, reason

		return result

	def editEvent(self, eventItem):
		"""
		This function takes an event item and edits any fields.  If the event could not be found in the system,
		returns with a warning and a reason that event was not found.
		"""

		events_file_path = GravityConfiguration.getEventsFile()
		gravity_events = JsonFile.readJsonFile( events_file_path )

		if "label" not in eventItem:
			reason = "Error: Could not edit event because we could not find the event label in eventItem request"
			return None, reason

		event_label = eventItem["label"]

		if gravity_events != None and "event_items" in gravity_events:
			events = gravity_events["event_items"]

			for event in events:

				print event_label
				if "label" in event and event["label"] == event_label:
					# We found the event, now edit the settings
					if "title" in eventItem:
						event["title"] = eventItem["title"]

					if "location" in eventItem:
						event["location"] = eventItem["location"]

					if "datetime" in eventItem:
						event["datetime"] = eventItem["datetime"]

					if "description" in eventItem:
						event["description"] = eventItem["description"]

					print "writing the fiel"
					result = JsonFile.writeJsonFile( gravity_events, events_file_path )

					if result == False:
						reason = "Error: Could not write blog file"
						print reason
						return None, reason
					else:
						return True, None

		reason = "Error: Could not find and edit event"
		return None, reason

	def deleteEvent(self, eventLabel):
		"""
		This deletes an event. Cannot be un-done.
		"""

		events_file_path = GravityConfiguration.getEventsFile()
		current_events = JsonFile.readJsonFile( events_file_path )

		if current_events == None:
			return None

		if "event_items" not in current_events:
			reason = "Warning: event_items not found in events file."
			print reason
			return None

		events = current_events["event_items"]

		index = 0
		for event in events:
			if "label" in event and event["label"] == eventLabel:
				print "Deleting event %s" % event["title"]
				del events[index]
				break

			index = index + 1

		result = JsonFile.writeJsonFile( current_events, events_file_path )

		if result == False:
			reason = "Error: Could not write events file"
			print reason
			return None, reason

		return True, None

	def getEvent(self, eventLabel):
		"""
		This gets a particular event based on the event label.
		"""

		events_file_path = GravityConfiguration.getEventsFile()
		gravity_events = JsonFile.readJsonFile( events_file_path )

		if gravity_events != None and "event_items" in gravity_events:
			events = gravity_events["event_items"]

			for event in events:
				if "label" in event and event["label"] == eventLabel:
					return event

		return None

	# **** Getters ****

	def getEventItems(self):

		events_file_path = GravityConfiguration.getEventsFile()
		current_events = JsonFile.readJsonFile( events_file_path )

		if current_events != None and "event_items" in current_events:

			# If there is no image set, set the default event image.
			for event in current_events["event_items"]:
				if "image" not in event:
					event["image"] = DEFAULT_ALT_IMAGE

			return current_events["event_items"]

		return None

if __name__ == '__main__':

	gravity_events = GravityEvents()

	gravity_events.createEvent( "Test Event", "12/1/2019 10:00 AM", "Washington, DC", "Be inspired by these world changers." )

