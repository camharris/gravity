#!/usr/bin/env python

"""
Written by: J. Patrick Farrell
Copyright 2019 Creative Collisions Technology, LLC

MessageParser.py

"""

# **** Local Imports ****
from JsonFile import *   # We are using this to read JSON files, should rename and separate the class.
from JsonMessage import *
from JsonSchemaHelper import *
from MessageHandlers import *
from UserManagement import *

ENFORCE_SCHEMA = True

class MessageParser():

	@staticmethod
	def parseMessage(message, options=None, logger=None):

		message_handlers = MessageHandlers(JsonMessage)

		message_response = None

		try:
			message_json = json.loads(message)

			if ENFORCE_SCHEMA == True:
				if JsonSchemaHelper.validateMessageSchema( message_json ) == False:

					message_response = JsonMessage.createErrorMessage( "reply", "None", "Failed Message Schema" )
					print "failed schema"
					return message_response

			if "command" not in message_json:
				"""
				We are expecting each message to have a command so we know what to do with it.  Otherwise return back a message
				to the client that we could not find the message object
				"""
				message_response = JsonMessage.createErrorMessage( "None", "Could not find command object in message" )
				return message_response

			else:

				command = message_json["command"]
				message_response = None

				if command == COMMAND_AJAX_TEST:

					message_response = JsonMessage.createResponseMessage( command )
					message_response = message_handlers.handleAjaxTest( message_json, message_response, logger )

				if command == "logout":

					# Check if the user passed the token they want to logout in the message.
					if "token" in message_json:
						username = UserManagement.getUsername( message_json["token"] )

						if username == None:
							message_response = JsonMessage.createErrorMessage( MSG_TYPE_REPLY, "logout", "Could not find the username for this token" )
							return message_response

						# Note that this function logs out the user with the current token, but does not remove all
						#  of that users tokens, only this specific one.
						result = UserManagement.logoutUser( message_json["token"] )

						if result == True:
							if username != None:
								logger.info( "Logged out user %s, users token removed." % username )

							message_response = JsonMessage.createResponseLogoutMessage()
							return message_response
						else:
							logger.info( "Logged out user %s, users token removed." % username )
							message_response = JsonMessage.createErrorMessage( MSG_TYPE_REPLY, "logout", "Token found in request but not valid, could not logout" )
							return message_response

						return

					elif "token" in options:

						# If the user didn't specify the token in the message, use it from the options field if it
						# is present
						token = options["token"]

						if token != None:
							username = UserManagement.getUsername( token )

							if username == None:
								message_response = JsonMessage.createErrorMessage( MSG_TYPE_REPLY, "logout", "Could not find the username for this token" )
								return message_response

							result = UserManagement.logoutUser( token )

							if result == True:
								if username != None:
									logger.info( "Logged out user %s" % username )

								message_response = JsonMessage.createResponseLogoutMessage()
								return message_response
							else:
								message_response = JsonMessage.createErrorMessage( MSG_TYPE_REPLY, "logout", "Token found in request by not valid, could not logout" )
								return message_response

							return

						else:
							# We couldn't find the token to logout the user.
							message_response = JsonMessage.createErrorMessage( MSG_TYPE_REPLY, "logout", "Token not found, could not logout" )
							return message_response

				elif command == "register":
					"""
					Here we are going to register a new user to the system.  After we register them, we log
					them in and send them back a token that they can use to access the system.
					"""

					if "user_info" in message_json:
						result, reason = UserManagement.addNewUser( message_json["user_info"] )

						if result == None:
							logger.error( reason )
							message_response = JsonMessage.createErrorMessage( "reply", "login", reason )
							return message_response
						else:

							username = message_json["user_info"]["username"]

							# Since we have now created a registered user, we should log them in
							# and give them their token so they can access the site.
							token = UserManagement.loginUser( username )

							logger.info( "Registered and logged in user %s" % username )

							# Now send the token back to the client
							json_response = JsonMessage.createResponseRegisterMessage( token )
							return json_response

				elif command == "login":

					if "user_login_info" in message_json:
						user_login_info = message_json["user_login_info"]

						if "username" in user_login_info:
							username = str(user_login_info["username"])

						if "password" in user_login_info:
							password = str(user_login_info["password"])

					if username == None or password == None:
						reason = "Username or password not provided to log user in"
						message_response = JsonMessage.createErrorMessage( MSG_TYPE_REPLY, "login", reason )
						return message_response

					# TODO: Don't like that reason = token when success, how to make this better?
					result, reason = UserManagement.verifyAndLoginUser( username, password )

					if result == False:
						logger.error( reason )
						message_response = JsonMessage.createErrorMessage( MSG_TYPE_REPLY, "login", reason )
						return message_response

					else:
						token = reason
						logger.info( "Logged in user %s" % username )
						# Now send the token back to the client
						message_response = JsonMessage.createResponseLoginMessage( token )
						return message_response

				elif command == COMMAND_BLOG_NEW_POST:
					message_response = message_handlers.handleSetBlogNewPost( message_json, logger )

				elif command == COMMAND_BLOG_SUBMIT_EDIT:
					message_response = message_handlers.handleBlogSubmitEdit( message_json, logger )

				elif command == COMMAND_BLOG_SUBMIT_DELETE:
					message_response = message_handlers.handleBlogSubmitDelete( message_json, logger )

				elif command == COMMAND_EVENT_SUBMIT_DELETE:
					message_response = message_handlers.handleEventSubmitDelete( message_json, logger )

				elif command == COMMAND_EVENT_SUBMIT_EDIT:
					message_response = message_handlers.handleEventSubmitEdit( message_json, logger )

				elif command == COMMAND_EVENT_SUBMIT_NEW:
					message_response = message_handlers.handleEventSubmitNew( message_json, logger )

				elif command == COMMAND_USER_ADD_NEW:
					message_response = message_handlers.handleUserAddNew( message_json, logger )

				elif command == COMMAND_USER_DELETE:
					message_response = message_handlers.handleUserDelete( message_json, logger )

				elif command == COMMAND_USER_EDIT:
					message_response = message_handlers.handleUserEdit( message_json, logger )

				elif command == COMMAND_USER_EDIT_PASSWORD:
					message_response = message_handlers.handleUserEditPassword( message_json, logger )


				elif command == COMMAND_USER_LOGOUT:
					message_response = message_handlers.handleUserLogout( message_json, logger )

				else:
					message_response = JsonMessage.createErrorMessage( "reply", "None", "Not find supported command" )

		except Exception, e:
			error = "parseMessage: e = %s" % e
			print error
			message_response = JsonMessage.createErrorMessage( "reply", "None", error )

		return message_response


if __name__ == '__main__':

	message = {}
	message["command"] = "ajax_test"
	message["value"] = "TEST!"
	message_response = MessageParser.parseMessage( json.dumps( message ) )
	print json.dumps(message_response)