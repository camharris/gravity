
Nginx
=====

Nginx is an opensource web server.  It can run on all platforms but will start with talking about OSX.

If you wish you run multiple websites with multiple instances of Gravity running at the same time, this is possible
on a Linux server by using the nginx server as your front end.  When you run multiple websites using Nginx as your front
end multiplexer for multiple websites, you actually will be disabling the internal HTTPS server within Gravity and using
Nginx to handle the SSL.

Install Nginx OSX
-----------------

[Original Resource](https://coderwall.com/p/dgwwuq/installing-nginx-in-mac-os-x-maverick-with-homebrew)

The easiest way to install nginx on OSX is through Homebrew.  To check if homebrew is installed, just type "brew"
on the command line.  If it isn't installed, check the instructions to install Homebrew.

To install nginx with homebrew:

	$ brew install nginx

Nginx Status
------------

	$ sudo systemctl status nginx

Restart

	$ sudo systemctl restart nginx

Configuring Nginx
-----------------

The default place of nginx.conf on Mac after installing with brew is:

	/usr/local/etc/nginx/nginx.conf


Docroot is: /usr/local/var/www

The default port has been set in /usr/local/etc/nginx/nginx.conf to 8080 so that
nginx can run without sudo.

nginx will load all files in /usr/local/etc/nginx/servers/.

To have launchd start nginx now and restart at login:
  brew services start nginx
Or, if you don't want/need a background service you can just run:
  nginx

==> Summary
  /usr/local/Cellar/nginx/1.12.2: 23 files, 1MB

Configuring for multiple sites
------------------------------

By default, nginx is configured only to server a single web site.  You can configure it to
server multiple websites by following the instructions below.  The sites-availalbe directory
contains a list of configuration files that can be used to configure nginx.

	$ mkdir -p /usr/local/etc/nginx/sites-available

The config files inside of sites-available are not enabled until you link them to the sites-enabled
directory.

	$ ln -s /usr/local/etc/nginx/sites-available/<config_file> /usr/local/etc/nginx/sites-enabled/<config_file>


Running Nginx on OSX
--------------------

Once nginx is installed, you will need to start the nginx daemon that runs the web server.

	$ sudo nginx

To stop nginx:

	$ sudo nginx -s stop


To test that the Nginx server is running, go to the following URL in a browser:

	http://localhost:8080


CORS
====

[Nginx Config](https://enable-cors.org/server_nginx.html)

[CORS Protocol](https://fetch.spec.whatwg.org/#cors-protocol)







