
SSL Implementation and Notes
============================

Most websites on the internet now are moving towards using HTTPS with an SSL certificate.  Google actually down ranks websites without HTTPS now and the "Not Secure" display within your browser doesn't look very professional.

Let's Encrypt
-------------

SSL certificates usually cost money.  But there is one Certificate Authority out there that you can use to get a free SSL certificate, called Let's Encrypt.

### Getting setup with Nginx on Ubuntu

[Setup Instructions](https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx)

	$ ln -s /etc/nginx/sites-available/creativecollisionstech /etc/nginx/sites-enabled/creativecollisionstech

### Debian Linux Instructions

Install certbot

	$ apt-get install certbot

Obtain certificate:

	$ sudo certbot --nginx

Renew certificate:

You need to use the email address you created the certificate with.

	$ certbot renew

Support SAN

	$ certbot certonly --webroot -w /var/www/html -d www.thegravityframework.com -d www.friendsofcrawfordpark.org -d www.mainedesigncompany.com -d www.netonos.com

Add domain to SAN list: just rerun above command with additional domain


