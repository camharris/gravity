
Gravity JSON Application Programmer Interface
---------------------------------------------

The Gravity Framework is designed with a built in API that both browsers use to interact with the server over POST requests, but third-party clients can also interact with the system as well though the same interface and message format.

THe most simple way to interact with the API is through a curl request.

### Curl Sample Request ###

curl is a command line program that can submit HTTP/HTTPS requests.  With this program, we can send a POST request containing a JSON command to the Gravity Server.  In these examples, we assume that the server is configured to interact through localhost (or 127.0.0.1), but you could do this across a network by providing the IP address of the host.

HTTP
	$ curl -k -H "Content-Type: application/json" -X POST -d '{"command": "ajax_test", "value": "TEST123"}' http://localhost/login

The response to this login will be something that looks like the following:

	{"output": "Gravity Server Received Input: TEST123", "command": "ajax_test", "msg_type": "reply"}
