
# Top-level Makefile for Gravity Server
# Copyright 2019
# Written by: J. Patrick Farrell

INSTALL_DIR=~/bin

IMAGE_NAME ?= docker.creativecollisionstech.com/cct/opensource/gravity

HOST_BASE_DIRECTORY=$(CURDIR)

# Location on the host machine where input and output files are placed
#  This is where files will be shared between containers.
HOST_CONFIG_DIRECTORY ?= ${HOST_BASE_DIRECTORY}/config
HOST_SECURITY_DIRECTORY ?= ${HOST_BASE_DIRECTORY}/security

# Locations inside of the image where upload and output files are referenced
IMAGE_CONFIG_DIRECTORY ?= /config
IMAGE_SECURITY_DIRECTORY ?= /security

# Host Configuration
HOST_HTTP_PORT ?= 80
HOST_HTTPS_PORT ?= 443

# Image Configuration
IMAGE_HTTP_PORT ?= 80
IMAGE_HTTPS_PORT ?= 443

VERSION ?= version0.1.1

all:
	make -C gravityApp

build:
	docker build -t $(IMAGE_NAME) .

clean:
	make -C gravityApp clean
	docker rmi ${IMAGE_NAME}

image-run:
	docker run --rm --name=gravity_server \
	-v ${HOST_CONFIG_DIRECTORY}:${IMAGE_CONFIG_DIRECTORY} \
	-v ${HOST_SECURITY_DIRECTORY}:${IMAGE_SECURITY_DIRECTORY} \
		-p ${HOST_HTTP_PORT}:${IMAGE_HTTP_PORT} \
		-p ${HOST_HTTPS_PORT}:${IMAGE_HTTPS_PORT} \
		${IMAGE_NAME}

push:
	docker push $(IMAGE_NAME)

pull:
	docker pull $(IMAGE_NAME)

tag:
	docker tag $(IMAGE_NAME) $(IMAGE_NAME):$(VERSION)

dev:
	docker-compose -f docker-compose.yml up

dev-d:
	docker-compose -f docker-compose.yml up -d

generate-key:
	openssl req -new -x509 -keyout ./security/server.pem -out ./security/server.pem -days 365 -nodes
