# Use an official Python runtime as a base image
FROM docker.creativecollisionstech.com/cct/cct_base_python

MAINTAINER Patrick Farrell <patrick@creativecollisionstech.com>
LABEL Description="Gravity Web Server" Vendor="creativecollisionstech" Version="0.1.1"

# Make port 80 available to the world outside this container
EXPOSE 80

# Set the working directory
WORKDIR /

# Copy the current directory contents into the container at /app
ADD ./gravityApp/app /app

# Add the HTML directory to the image
ADD ./public /public

# Add the templates directory to the image
ADD ./templates /templates

# Run app.py when the container launches
CMD ["python", "app/GravityServer.py"]